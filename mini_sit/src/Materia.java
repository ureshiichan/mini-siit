/**
 *
 * @author
 * Jonathan
 * Samantha
 * Cristian
 */
public class Materia {
    
    public String nombre, clave;
    public int creditos;
    public static final String [] atributosMateria = {"clave","nombre","creditos"};
    public Materia cadena;
    public Lista <Alumno2> listaAlumnos;

    public Materia(String nombre, String clave, int creditos) {
        this.nombre = nombre;
        this.clave = clave;
        this.creditos = creditos;
        listaAlumnos = new Lista();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Materia getCadena() {
        return cadena;
    }

    public void setCadena(Materia cadena) {
        this.cadena = cadena;
    }

    public Lista<Alumno2> getListaAlumnos() {
        return listaAlumnos;
    }

    public void setListaAlumnos(Lista<Alumno2> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }
    
}