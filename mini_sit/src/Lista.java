
import java.lang.reflect.Field;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 * @authors
 * Jonathan
 * Samantha
 * Cristian
 */
// Agregar ordenado sin repetir numero de control
//  Eliminar especial
// consulta 
// reporte (cuando no le aplicas filtro)
/*
* Esta clase contendra principalmente los algortitmos que son usados dentro de una lista
 */
public class Lista<Type> {

    private Type dato, w, u, r, a ,aux;
    
    private Lista<Type> liga;

    public Lista() {
        this.dato = null;
        this.w = null;
        this.u = null;
        this.r = null;
        this.a = null;
        this.aux = null;
    }

    public boolean agregarInicio(Type dato) {
        if (listaVacia()) {
            this.dato = dato;
            return true;
        }
        return false;
    }
    
    /* Método para crear un nodo en caso de que la lista este vacia 
        Primero hace una comparación con 'dato', en caso de que este vacio agrega de 
        inmediato un dato, en este caso del tipo alumno.
    */
    public boolean crear(Type nodo){
        if(dato == null){
            dato = nodo;
            u = nodo;
            //((Nodo)nodo).ligaDerecha = null;
            return false;
        }else{
            return true;
        }
    }
    
    /* Método para eliminar el primer nodo */
    public void EliminarPrimerNodo(){
        if(!listaVacia()){
            if(dato == u){
                aux = dato;
                dato = null;
                u = null;
            }else{
                aux = dato;
                dato = (Type) ((Nodo)dato).ligaDerecha;
            }
        }
    }
    
    /* Método para eliminar el último nodo */
    public void EliminarUltimoNodo(){
        r = dato;
        while(r != null){
            if(r == u){
                if (r == dato){
                    aux = r;
                    dato = null;
                    u = null;
                }else{
                    aux = r;
                    //r = null;
                    ((Nodo)a).ligaDerecha = null;
                    }
            }
            a = r;
            r = (Type) ((Nodo)r).ligaDerecha;
        }
    }
    
    /* Método para verificar que realmente exista el dato */
    public boolean existe(String atributo, String ref) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        if(!listaVacia()){
            /* Se obtiene el campo identificador de la clase de la lista */
            Field f = dato.getClass().getField(atributo);
            /* bandera iniciada en falso, es decir, el dato no se ha encontrado */
            boolean bandera = false;
            r = dato;
            /* Recorrer la lista tomando como punto de partida 'r = dato' */
            while(r!=null){
                /* Verificamos si es el dato que se esta buscando */
                if(f.get(r).equals(ref)){
                    /* Si se encuentra el dato, se modifica la bandera*/
                    bandera = true;
                    break;
                }
                /* Seguir recorriendo la lista */
                r = (Type)((Nodo) r).ligaDerecha;
            }
            return bandera;
        }else return false;
    }
    
    /* Método para eliminar un dato en especifico */
    public void EliminarPorReferencia(String atributo, String ref) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        if(existe(atributo, ref)){
            r = dato;
            a = null;
            Field f = dato.getClass().getField(atributo);
            while(r != null){
                if(f.get(r).equals(ref)){
                    /* Si el dato a eliminar es el primero */
                    if( r == dato ){
                        EliminarPrimerNodo();
                        break;
                    }
                    /* Si el dato a eliminar es el último */
                    else if ( r == u ){
                        EliminarUltimoNodo();
                        break;
                    }else{
                       aux = r;
                       ((Nodo)a).ligaDerecha = ((Nodo)r).ligaDerecha;
                       r = (Type)((Nodo)r).ligaDerecha;
                       break;
                    }
                }else{
                    a = r;
                    r = (Type) ((Nodo)r).ligaDerecha;
                }
            }
        }
    }
    
    /* Método para consultar un dato en especifico */
    public Type consultar(String atributo, String ref) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        if(!listaVacia()){
            /* Se obtiene el campo identificador de la clase de la lista */
            Field f = dato.getClass().getField(atributo);
            Type e = null;
            r = dato;
            /* Ciclo para recorrer la lista */
            while(r != null){
                /* Verificamos que sea el dato que se está buscando */
                if(f.get(r).equals(ref)){
                    /* Modificamos e, en este caso retornará todos los datos del alumno */
                    e = r;
                }
                r = (Type)((Nodo)r).ligaDerecha;
            }
            return e;
        }else
            return null;
    }
    
    /* Método para agregar al final un dato */
    public void agregarAlFinal(Type nodo){
        if(crear(nodo)){
            //((Nodo)u).ligaDerecha = nodo;
            u = nodo;
            //((Nodo)u).ligaDerecha = null;
        }
    }

    /* Si la lista se encuentra vacia, retorna un booleano */
    private boolean listaVacia() {
        return (this.dato == null);
    }
    
    /* Método para mostrar en la tabla todos los valores */
    /* El vector 'atributos' viene definido en cada una de las clases */
    void mostrar(JTable jTable1, String atributos[])  throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        DefaultTableModel model = (DefaultTableModel)jTable1.getModel();
        if(!listaVacia()){
            r = dato;
            int i = 0;
            while(r != null){
                for(int j = 0; j<atributos.length; j++){
                    Field f = r.getClass().getField(atributos[j]);
                    model.setValueAt(f.get(r), i, j);
                }
                r = (Type) ((Nodo)r).ligaDerecha;
                i++;
            }
        }
        
    }

}
