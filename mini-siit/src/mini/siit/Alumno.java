package mini.siit;

import java.util.concurrent.ThreadLocalRandom;

public class Alumno {

    public String noControl;
    public String Nombre;
    public int sem;
    public int[][] calif; //[si la esta llevando ahora(Para el promedio)][calificacion de la materia]

    public Alumno(String noControl, String Nombre) {
        this.noControl = noControl;
        this.Nombre = Nombre;
        this.sem = ThreadLocalRandom.current().nextInt(1, 2);
        //Cuantas listas van a haber?
        /*
          Primero 2017 (sem=1)       Segundo 2017(Sem=2)
        0 F.P                                   0 FP (ya con un >70 fijo)
        1 M.D                                 1 M.D (ya con un >70 fijo)
        2 Etica                               2 Etica (ya con un >70 fijo)
        3 Poo                                 3 Poo              
        4 Conta                             4 Conta
        5 F.P R                                5 E.D  
        6 M.D R                              6 C.E
        7 Etica R                             7 POO R
        ..............                              8 Conta R
         */
        if (this.sem == 1) { //Todos los de 1 sem 2017  tendran todas en cero
            this.calif = new int[7][2];
        }
        if (this.sem == 2) { //y los de sem 2 2017 ya iniciaran con las primeras tres materias pasadas
            this.calif = new int[8][2];

            for (int i = 0; i < 3; i++) {
                this.calif[i][0] = 0; //Directamente se va a marcar que no se está cursando (0= falso)
                this.calif[i][1] = ThreadLocalRandom.current().nextInt(70, 100); //Se genera una califiacion aprobatoria del 70 al 100
            }
        }
    }

    public void generarCalif() {
        ThreadLocalRandom.current().nextInt(0, 100);
    }

}