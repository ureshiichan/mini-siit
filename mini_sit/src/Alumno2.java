/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author
 * Jonathan
 * Samantha
 * Cristian
 */
public class Alumno2 extends Nodo{
    public String NoControl;
    public String nombreCompleto;
    
    public int creditos;
    public int semestre;
    public float promedioAcumulado;
    public Lista <Materia> materias;
    
    public static final String[] datosAlumno = {"NoControl","NombreCompleto"};

    public Alumno2(String no_control, String nombre, int creditos, int semestre, float promedioAcumulado) {
        this.NoControl = no_control;
        this.nombreCompleto = nombre;
        this.creditos = creditos;
        this.semestre = semestre;
        this.promedioAcumulado = promedioAcumulado;
        this.materias= new Lista();
    }

    public String getNoControl() {
        return NoControl;
    }

    public void setNoControl(String NoControl) {
        this.NoControl = NoControl;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public float getPromedioAcumulado() {
        return promedioAcumulado;
    }

    public void setPromedioAcumulado(float promedioAcumulado) {
        this.promedioAcumulado = promedioAcumulado;
    }

    public Lista<Materia> getMaterias() {
        return materias;
    }
}
